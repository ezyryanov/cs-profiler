//
//  SteamLogin.swift
//  SteamLogin
//
//  Created by Serhii Londar on 12/25/18.
//

import Foundation
import UIKit

public class SteamLogin {
    public static var steamApiKey: String = "96EE4123D62E642A6E367D0A28D4682D"
    
    class func login(from vc: BaseRootViewController, completion: @escaping SteamLoginVCHandler) {
        let loginVC = AuthViewController(loginHandler: completion)
        vc.present(loginVC, animated: true, completion: nil)
    }
}
