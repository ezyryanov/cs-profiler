//
//  BaseCoordinator.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 20.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit

class BaseCoordinator: NSObject {
    unowned var rootController : UIViewController?
    var currentController : UIViewController?
    
    func showAuth(success : @escaping (SteamUser?,Error?)->()) {
        let authVC = AuthViewController(loginHandler: success)
        authVC.coordinator = self
        authVC.hidesBottomBarWhenPushed = true
        self.currentController?.navigationController?.setNavigationBarHidden(true, animated: false)
        self.currentController?.navigationController?.pushViewController(authVC, animated: false)
    }
}
