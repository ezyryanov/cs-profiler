//
//  BaseViewController.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 21.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit

class BaseChildViewController: UIViewController {

    unowned var coordinator : BaseCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
