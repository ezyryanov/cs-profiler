//
//  BaseRootViewController.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 21.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import RealmSwift

class BaseRootViewController: UIViewController {

    var coordinator : BaseCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.coordinator?.currentController = self
        if (UserDefaults.standard.object(forKey: "SteamIDKey") == nil) {
            self.coordinator?.showAuth(success: { (steamUser, error) in
                guard let userID = steamUser?.steamID64 else { return }
                UserDefaults.standard.set(userID, forKey: "SteamIDKey")
            })
        } else {

        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
