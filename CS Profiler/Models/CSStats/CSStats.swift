//
//  CSStats.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 24.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit

class CSStatsResponse: Codable {
    var playerstats : CSStats
}

class CSStats: Codable {
    var steamID : String
    var gameName : String
    var stats : [StatItem]
    var achievements : [AchievementItem]
}
