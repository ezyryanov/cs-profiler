//
//  AchievementItem.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 24.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import RealmSwift

class AchievementItem: Object, Codable {
    var name : String?
    var achieved : Int?
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
