//
//  StatItem.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 24.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import RealmSwift

class StatItem: Object, Codable {
    var name : String?
    var value : Int?
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
