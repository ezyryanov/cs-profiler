//
//  Friends.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 24.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import RealmSwift

class FriendsResponse: Codable {
    var friendslist : Friends
}

class Friends: Codable {
    var friends : [Friend]
}

class Friend: Object, Codable {
    var steamid : String?
    var relationship : String?
    var friend_since :  Int?
    
    override static func primaryKey() -> String? {
        return "steamid"
    }
}
