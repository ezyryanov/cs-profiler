//
//  PlayerInfo.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 23.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import RealmSwift

class PlayersResponse: Codable {
    var response : Players
}

class Players: Codable {
    var players : [PlayerInfo]
}

class PlayerInfo: Object, Codable {
    var avatar : String?
    var avatarfull : String?
    var avatarmedium : String?
    var commentpermission : Int?
    var communityvisibilitystate : Int?
    var lastlogoff : Int?
    var loccityid : Int?
    var loccountrycode : String?
    var locstatecode : String?
    var personaname : String?
    var personastate : Int?
    var personastateflags : Int?
    var primaryclanid : String?
    var profilestate : Int?
    var profileurl : String?
    var realname : String?
    var steamid : String?
    var timecreated : Int?
    
    override static func primaryKey() -> String? {
        return "steamid"
    }
}
