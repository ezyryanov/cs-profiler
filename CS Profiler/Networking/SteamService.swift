//
//  SteamService.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 20.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

enum ApiMethod : String {
    case getPlayerSummaries = "GetPlayerSummaries"
    case getFriendList = "GetFriendList"
    case getUserStatsForGame = "GetUserStatsForGame"
}

extension ApiMethod : CaseIterable {}

class SteamService: NSObject {

static let shared = SteamService()
 
    
let apiBaseUrlV1User = "https://api.steampowered.com/ISteamUser/%@/v0001/?key=\(SteamLogin.steamApiKey)"
let apiBaseUrlV2User = "https://api.steampowered.com/ISteamUser/%@/v0002/?key=\(SteamLogin.steamApiKey)"
let apiBaseUrlV1Stats = "https://api.steampowered.com/ISteamUserStats/%@/v1/?key=\(SteamLogin.steamApiKey)"
let apiBaseUrlV2Stats = "https://api.steampowered.com/ISteamUserStats/%@/v2/?key=\(SteamLogin.steamApiKey)"
let appID = "730"
    
static let session =  URLSession(configuration: .default)
    
    
func getPlayerInfo(steamID : String = UserDefaults.standard.object(forKey: "SteamIDKey") as! String) {
    let argumentsString = "&steamids=\(steamID)"
    let urlString = String(format: apiBaseUrlV2User, arguments: [ApiMethod.getPlayerSummaries.rawValue]) + argumentsString
    AF.request(urlString, headers: nil).responseDecodable(of: PlayersResponse.self) { response in
        do {
            let realm = try Realm()
            if let playersArray = response.value?.response.players {
                try realm.write {
                    for player in playersArray {
                        realm.add(player, update: .all)
                    }
                }
            }
        } catch let error as NSError {
            print("Handle \(error)")
        }
    }
}
    
func getFriendsList(steamID : String = UserDefaults.standard.object(forKey: "SteamIDKey") as! String) {
    let argumentsString = "&steamid=\(steamID)&relationship=friend"
    let urlString = String(format: apiBaseUrlV1User, arguments: [ApiMethod.getFriendList.rawValue]) + argumentsString
    AF.request(urlString, headers: nil).responseDecodable(of: FriendsResponse.self) { response in
        do {
            let realm = try Realm()
            if let friendsArray = response.value?.friendslist.friends {
                try realm.write {
                    for friend in friendsArray {
                        realm.add(friend, update: .all)
                    }
                }
            }
        } catch let error as NSError {
            print("Handle \(error)")
        }
    }

}
    
func getGameStats(steamID : String = UserDefaults.standard.object(forKey: "SteamIDKey") as! String) {
    let argumentsString = "&steamid=\(steamID)&appid=\(appID)"
    let urlString = String(format: apiBaseUrlV2Stats, arguments: [ApiMethod.getUserStatsForGame.rawValue]) + argumentsString
    AF.request(urlString, headers: nil).responseDecodable(of: CSStatsResponse.self) { response in
        do {
            let realm = try Realm()
            if let statsArray = response.value?.playerstats.stats {
                try realm.write {
                    for statItem in statsArray {
                        realm.add(statItem, update: .all)
                    }
                }
            }
            if let achievementsArray = response.value?.playerstats.achievements {
                try realm.write {
                    for achievement in achievementsArray {
                        realm.add(achievement, update: .all)
                    }
                }
            }
        } catch let error as NSError {
            print("Handle \(error)")
        }
    }
}
    
}
