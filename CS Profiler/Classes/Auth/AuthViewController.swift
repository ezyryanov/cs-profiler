//
//  AuthViewController.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 20.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import Foundation
import WebKit

public typealias SteamLoginVCHandler = (_ user: SteamUser?, _ error: Error?) -> Void

private let STEAM_MOBILE_LOGIN_URL = "https://steamcommunity.com/mobilelogin"
public let defaultNavigationBarColor = UIColor(red: 23.0 / 255.0, green: 26.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)

class AuthViewController: BaseChildViewController {
    enum Strings: String {
        case cancel = "Cancel"
        case loginToSteam = "Login to Steam"
        case initError = "init(coder:) has not been implemented"
    }
    
    var loginWebView: WKWebView!
    
    var loginHandler: SteamLoginVCHandler
    
    public class func login(from vc: UIViewController, navigationBarColor: UIColor = defaultNavigationBarColor, completion: @escaping SteamLoginVCHandler) {
        let loginVC = AuthViewController(loginHandler: completion)
        let navigationVC = UINavigationController(rootViewController: loginVC)
        vc.present(navigationVC, animated: true, completion: nil)
    }
    
    public init(loginHandler: @escaping SteamLoginVCHandler) {
        self.loginHandler = loginHandler
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError(Strings.initError.rawValue)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.addWebView()
        self.title = Strings.loginToSteam.rawValue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    func addWebView() {
        loginWebView = WKWebView(frame: self.view.frame)
        loginWebView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loginWebView)
        
        let topConstraint = NSLayoutConstraint(item: loginWebView!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
        topConstraint.isActive = true
        self.view.addConstraint(topConstraint)
        let leftConstraint = NSLayoutConstraint(item: loginWebView!, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0)
        leftConstraint.isActive = true
        self.view.addConstraint(leftConstraint)
        let bottomConstraint = NSLayoutConstraint(item: loginWebView!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        bottomConstraint.isActive = true
        self.view.addConstraint(bottomConstraint)
        let rightConstraint = NSLayoutConstraint(item: loginWebView!, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0)
        rightConstraint.isActive = true
        self.view.addConstraint(rightConstraint)
        
        loginWebView.navigationDelegate = self
        loginWebView.load(URLRequest(url: URL(string: STEAM_MOBILE_LOGIN_URL)!))
    }
    
    @objc public func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension AuthViewController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url
        if ((url?.absoluteString as NSString?)?.range(of: "steamcommunity.com/profiles/"))?.location != NSNotFound {
            let urlComponents = url?.absoluteString.components(separatedBy: "/")
            let potentialID: String = urlComponents![4]
            let lUser = SteamUser(steamID64: potentialID)
            loginHandler(lUser, nil)
            self.navigationController?.popViewController(animated: true)
            decisionHandler(.cancel)
        } else if ((url?.absoluteString as NSString?)?.range(of: "steamcommunity.com/id/"))?.location != NSNotFound {
            let urlComponents = url?.absoluteString.components(separatedBy: "/")
            let potentialVanityID: String = urlComponents![4]
            let lUser = SteamUser(steamVanityID: potentialVanityID)
            loginHandler(lUser, nil)
            self.navigationController?.popViewController(animated: true)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loginHandler(nil, error)
        dismiss(animated: true, completion: nil)
    }
}
