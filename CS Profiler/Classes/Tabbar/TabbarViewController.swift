//
//  TabbarViewController.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 20.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let profileViewController = ProfileViewController()
        profileViewController.coordinator = ProfileCoordinator()
        let profileNavigationController = UINavigationController(rootViewController: profileViewController)
        profileNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        
        let informationViewController = InformationViewController()
        informationViewController.coordinator = InformationCoordinator()
        let informationNavigationController = UINavigationController(rootViewController: informationViewController)
        informationNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)
        
        let sessionsViewController = SessionsViewController()
        sessionsViewController.coordinator = SessionsCoordinator()
        let sessionsNavigationController = UINavigationController(rootViewController: sessionsViewController)
        sessionsNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 2)

        let tabBarList = [profileNavigationController, informationNavigationController,sessionsNavigationController]

        viewControllers = tabBarList
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
