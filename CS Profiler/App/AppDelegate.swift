//
//  AppDelegate.swift
//  CS Profiler
//
//  Created by Egor Zyryanov on 19.11.2019.
//  Copyright © 2019 Egor Zyryanov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let tabbbarViewController = TabbarViewController()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = tabbbarViewController
        self.window?.backgroundColor = .white
        self.window?.makeKeyAndVisible()

        return true
    }

}

